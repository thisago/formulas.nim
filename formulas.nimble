# Package

version       = "0.1.0"
author        = "Thisago"
description   = "Mathematical formulas"
license       = "MIT"
srcDir        = "src"


# Dependencies

requires "nim >= 1.0.0"
