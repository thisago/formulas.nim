##[
  :Author: Thiago Navarro
  :Email: thiago@oxyoy.com

  **Created at:** 06/15/2021 10:18:17 Tuesday
  **Modified at:** 06/16/2021 10:50:13 AM Wednesday

  ------------------------------------------------------------------------------

  formulas
  ------------------------------------------------------------------------------

  Formulas
]##


import ./formulas/tetrahedron
export tetrahedron

import ./formulas/cone
export cone
