##[
  :Author: Thiago Navarro
  :Email: thiago@oxyoy.com

  **Created at:** 06/15/2021 10:16:42 Tuesday
  **Modified at:** 06/17/2021 09:43:05 AM Thursday

  ------------------------------------------------------------------------------

  base
  ------------------------------------------------------------------------------

  Base module for all other formulas
]##

type
  Shape* = object of RootObj
